console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function welcomeMessage(){
		let fullName = prompt("What is your name?");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live?");
		
		console.log("Hello, "  + " "  + fullName);
		console.log("You are " + " " + age + " years old.");
		console.log("You live in " + " " + location);
	};

	welcomeMessage();

	function sampleAlert(){
	alert('Thank you for your input!');
	}

	sampleAlert();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayBands(){
		console.log("1. Ben & Ben");
		console.log("2. December Avenue");
		console.log("3. The Juan");
		console.log("4. Rivermaya");
		console.log("5. Coldplay");
	};

	displayBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printMovies(){
		let movieOne = '1. Black Panther 2018';
		let ratingOne = '96%';
		let movieTwo = '2. Coco 2017'; 
		let ratingTwo = '97%';
		let movieThree = '3. Inside Out 2015';
		let ratingThree = '98%';
		let movieFour = '4. Wonder Woman 2017';
		let ratingFour = '93%';
		let movieFive = '5. Parasite 2019';
		let ratingFive = '99%';

		console.log(movieOne);
		console.log('Rotten Tomatoes Rating is: ' + ratingOne);
		console.log(movieTwo);
		console.log('Rotten Tomatoes Rating is: ' + ratingTwo);
		console.log(movieThree);
		console.log('Rotten Tomatoes Rating is: ' + ratingThree);
		console.log(movieFour);
		console.log('Rotten Tomatoes Rating is: ' + ratingFour);
		console.log(movieFive);
		console.log('Rotten Tomatoes Rating is: ' + ratingFive);
	};

	printMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// 	let printFriends() = 
	// 	let friend1 = alert("Enter your first friend's name:"); 
	// 	let friend2 = prompt("Enter your second friend's name:"); 
	// 	let friend3 = prompt("Enter your third friend's name:");

	// 	console.log("You are friends with:")
	// 	console.log(friend1); 
	// 	console.log(friend2); 
	// 	console.log(friends); 
	// };


	// 	console.log(friend1);
	// 	console.log(friend2);



	function printUsers(){
	
		alert("Hi! Please add the names of your friends.");
	};

	printUsers();

	function printFriends(){
		let friend1 = prompt("Enter your first friend's name:");
		let friend2 = prompt("Enter your second friend's name:");
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:");
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};

	printFriends();



